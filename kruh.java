/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */


import java.util.*;

/*******************************************************************************
 * Instance třídy {@code kruh} představují ...
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class kruh
{
  private double polomer;
  private double prumer;
  

  public void setPolomer (int polomer)
    {
      this.polomer = polomer;
    }
    
  public void setPrumer (int prumer)
    {
      this.prumer = prumer;
    }
    
  public String getInfo ()
   {
      return "Aktuální poloměr je " + polomer + " a průměr " + prumer + ".";
   }
   
  public static double Obsah (double polomer)
   {
      return (Math.PI * Math.pow (polomer, 2));
   }

   public String getInfo2 ()
   {
      return "Aktuální Obsah je " + 666 +".";
   } 
}
   